#include "HelloWorldScene.h"

USING_NS_CC;

HelloWorld* HelloWorld::singleton = NULL;

CCScene* HelloWorld::scene()
{
    CCScene *scene = CCScene::create();
    HelloWorld *layer = HelloWorld::create();
    scene->addChild(layer);
    return scene;
}

bool HelloWorld::init()
{
    if (!CCLayer::init()) return false;
    
    m_shift = 0;
    m_scroll = 0;
    m_dir = CCDirector::sharedDirector();
    m_screen = m_dir->getWinSize();
    
    m_space = cpSpaceNew();
    m_space->gravity = cpv(0, -50);
    
    m_rt = CCRenderTexture::create(512, 512);
    m_rt->setPosition(m_screen * 0.5f);
    m_rt->setScale(0.5f);
    m_rt->retain();
    addChild(m_rt);
    _generateBackground();
    
    m_sky = Sky::create();
    m_sky->setTexture(CCTextureCache::sharedTextureCache()->addImage("hills.jpg"));
    m_sky->setContentSize(m_screen);
    m_sky->retain();
    addChild(m_sky);
    
    m_terrain = Terrain::create();
    m_terrain->setTexture(m_rt->getSprite()->getTexture());
    m_terrain->setContentSize(m_screen);
    m_terrain->initHills(m_screen.height * 0.3, m_screen.height * 0.5, m_space);
    m_terrain->retain();
    addChild(m_terrain);
    
    m_treesLayer = CCLayer::create();
    addChild(m_treesLayer);
    for(int i = 0; i < 100; i++)
    {
        int idx = floor(CCRANDOM_0_1() * 7);
        CCSprite *tree = CCSprite::create(CCString::createWithFormat("tree-blur%02d.png", idx)->getCString());
        tree->setAnchorPoint(ccp(0.5f, 0));
        //tree->setScale(0.15);
        tree->setPosition(ccp(CCRANDOM_0_1()*100000, 0));
        m_treesLayer->addChild(tree);
    }
    
    m_rain = Sky::create();
    m_rain->setTexture(CCTextureCache::sharedTextureCache()->addImage("rain.png"));
    m_rain->setContentSize(m_screen);
    m_rain->retain();
    m_rain->setTile(2);
    addChild(m_rain);
    
    m_grass = Sky::create();
    m_grass->setTexture(CCTextureCache::sharedTextureCache()->addImage("grass.png"));
    m_grass->setTile(1);
    m_grass->setContentSize(CCSizeMake(m_screen.width, m_screen.height * 0.3f));
    m_grass->retain();
    addChild(m_grass);
    
    m_wheel = CCPhysicsSprite::create("wheel.png");
    m_wheel->setScale(0.2);
    m_wheel->setIgnoreBodyRotation(false);
    m_wheel->setCPBody(cpBodyNew(1, cpMomentForCircle(1, 0, m_wheel->getContentSize().width*0.1, cpvzero)));
    cpSpaceAddBody(m_space, m_wheel->getCPBody());
    cpShape *circ = cpCircleShapeNew(m_wheel->getCPBody(), m_wheel->getContentSize().width*0.1, cpvzero);
    circ->e = 0.5;
    circ->u = 1;
    //circ->collision_type = COLL_PG;
    cpSpaceAddShape(m_space, circ);
    //cpBodyApplyImpulse(m_wheel->getCPBody(), cpv(50, 0), cpv(500, 0));
    m_wheel->setPosition(m_screen * 0.5);
    m_terrain->addChild(m_wheel);
    
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    //m_terrain->addChild(CCPhysicsDebugNode::create(m_space), 10);
#endif
    
    schedule(schedule_selector(HelloWorld::_update));
    setTouchEnabled(true);
    
    return true;
}

void HelloWorld::_update(float dt)
{
    m_scroll = -m_wheel->getPositionX() + m_screen.width * 0.3;
    m_terrain->setPositionX(m_scroll);
    m_treesLayer->setPositionX(m_scroll * 2);
    m_rain->setOffset(m_rain->getOffset() + ccp(dt * 0.1f, dt * -0.15f) * 10);
    m_sky->setOffset(ccp(-m_scroll * 0.0001f, 0));
    m_grass->setOffset(ccp(-m_scroll * 0.005f, 0));
    
    // update physics
    const int iterations = 10;
    for (int i = 0; i < iterations; i++)
    {
        cpSpaceStep(m_space, 0.01f);
    }
}

void HelloWorld::_generateBackground()
{
    float w = 512;
    float h = 512;
    ccVertex2F vp[] = {
        vertex2(0, h), vertex2(w, h),
        vertex2(0, 0), vertex2(w, 0)
    };
    ccColor4F vc[] = {
        ccc4f(0,0,0,1), ccc4f(0,0,0,1),
        ccc4f(0,0,0,0), ccc4f(0,0,0,0)
    };
//    int vi[] = {
//        0,1,2,
//        1,3,2
//    };
    
    CCSprite *m_noise = CCSprite::create("noise.png");
    m_noise->setPosition(ccp(w,h) * 0.5f);
    m_noise->setBlendFunc((ccBlendFunc){GL_DST_COLOR, GL_ZERO});

    // begin draw-to-texture
    m_rt->beginWithClear(1, 1, 1, 1);
    
    // draw the noise
    m_noise->visit();
    
    // draw the gradient
    CCGLProgram *shader = CCShaderCache::sharedShaderCache()->programForKey(kCCShader_PositionColor);
    shader->use();
    shader->setUniformsForBuiltins();
    glVertexAttribPointer(kCCVertexAttrib_Position, 2, GL_FLOAT, GL_FALSE, 0, vp);
    glVertexAttribPointer(kCCVertexAttrib_Color, 4, GL_FLOAT, GL_TRUE, 0, vc);
    ccGLBlendFunc(CC_BLEND_SRC, CC_BLEND_DST);
    //glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, vi);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    
    m_rt->end();
}
bool HelloWorld::ccTouchBegan(CCTouch *pTouch, CCEvent *pEvent)
{
    dragStart = pTouch->getLocation();
    //cpBodyApplyImpulse(m_wheel->getCPBody(), cpv(50, 30), cpvzero);
    m_wheel->getCPBody()->w = -10;
    m_shift = 0;
    return true;
}

void HelloWorld::ccTouchMoved(CCTouch *pTouch, CCEvent *pEvent)
{
    CCPoint diff = pTouch->getLocation() - dragStart;
    m_shift = -diff.x;
}

void HelloWorld::ccTouchEnded(CCTouch *pTouch, CCEvent *pEvent)
{
    
}
