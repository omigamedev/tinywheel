#include "Sky.h"

void Sky::setTexture(CCTexture2D *texture)
{
    tex = texture;
    ccTexParams tp = {GL_LINEAR, GL_LINEAR, GL_REPEAT, GL_REPEAT};
    tex->setTexParameters(&tp);
    CCGLProgram *shader = CCShaderCache::sharedShaderCache()->programForKey(kCCShader_PositionTexture);
//    shader->setUniformLocationWith1i(kCCUniformSampler, tex->getName());
    setShaderProgram(shader);
    m_offset = CCPointZero;
    m_tile = 1;
}

void Sky::draw()
{
    float w = getContentSize().width;
    float h = getContentSize().height;
    ccVertex2F vp[] = {
        vertex2(0, h), vertex2(w, h),
        vertex2(0, 0), vertex2(w, 0)
    };
    ccTex2F vt[] = {
        tex2(0+m_offset.x, m_offset.y), tex2(m_tile+m_offset.x, m_offset.y),
        tex2(0+m_offset.x, m_tile+m_offset.y), tex2(m_tile+m_offset.x, m_tile+m_offset.y)
    };
//    unsigned int vi[] = {
//        0,1,2,
//        1,3,2
//    };
    
    // draw the gradient
    CCGLProgram *shader = getShaderProgram();
    shader->use();
    shader->setUniformsForBuiltins();
    ccGLEnableVertexAttribs(kCCVertexAttribFlag_Position|kCCVertexAttribFlag_TexCoords);
    ccGLBindTexture2D(tex->getName());
    glVertexAttribPointer(kCCVertexAttrib_Position, 2, GL_FLOAT, GL_FALSE, 0, vp);
    glVertexAttribPointer(kCCVertexAttrib_TexCoords, 2, GL_FLOAT, GL_FALSE, 0, vt);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
}
