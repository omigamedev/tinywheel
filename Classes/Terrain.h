#include "cocos2d.h"
#include "chipmunk.h"
#include <vector>

using namespace cocos2d;
using namespace std;

class Terrain : public CCNode
{
    CCTexture2D *tex;
    vector<CCSprite*> trees;
    float hills[100];
    int treemap[100];
    
    CCPoint _interpolate(int i, float t);
    CCPoint _interpolateNormal(int i, float t);
    
public:
    float texOffset;
    
    CREATE_FUNC(Terrain);
    
    void setTexture(CCTexture2D *texture);
    void initHills(float height, float middle, cpSpace *sp);
    virtual void draw();
};
