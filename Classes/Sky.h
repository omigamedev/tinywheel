#include "cocos2d.h"

using namespace cocos2d;

class Sky : public CCNode
{
    CCTexture2D *tex;
    CCPoint m_offset;
    float m_tile;
public:
    CREATE_FUNC(Sky);
    
    void setTexture(CCTexture2D *texture);
    const CCPoint& getOffset() { return m_offset; }
    void setOffset(const CCPoint &off)
    {
        m_offset = off;
        while(m_offset.x > 1.0f)
            m_offset.x -= 1.0f;
        while(m_offset.x < -1.0f)
            m_offset.x += 1.0f;
        while(m_offset.y > 1.0f)
            m_offset.y -= 1.0f;
        while(m_offset.y < -1.0f)
            m_offset.y += 1.0f;
    }
    void setTile(float tile) { m_tile = tile; }
    virtual void draw();
};
