#include "Terrain.h"

void Terrain::setTexture(CCTexture2D *texture)
{
    tex = texture;
    ccTexParams tp = {GL_LINEAR, GL_LINEAR, GL_REPEAT, GL_REPEAT};
    tex->setTexParameters(&tp);
    CCGLProgram *shader = CCShaderCache::sharedShaderCache()->programForKey(kCCShader_PositionTexture);
//    shader->setUniformLocationWith1i(kCCUniformSampler, tex->getName());
    setShaderProgram(shader);
    texOffset = 0;
}

void Terrain::initHills(float height, float middle, cpSpace *sp)
{
    float sgn = 1;
    for(int i = 0; i < 100; i++)
    {
        hills[i] = sgn * CCRANDOM_0_1() * height + middle;
        if(i % 3 == 0)sgn *= -1;
    }
    int oldn = 0;
    for(int i = 0; i < 99; i++)
    {
        int n = floor(CCRANDOM_0_1() * 7);
//        CCPoint p1 = _interpolate(i, 0);
//        CCPoint p2 = _interpolate(i+1, 0);
//        cpShape *seg = cpSegmentShapeNew(sp->staticBody, cpv(p1.x,p1.y), cpv(p2.x,p2.y), 0);
//        //seg->collision_type = COLL_WALL;
//        cpSpaceAddStaticShape(sp, seg);
        for(int j = 0; j < 10; j++)
        {
            float t1 = j / 10.0f;
            float t2 = (j+1) / 10.0f;
            CCPoint p1 = _interpolate(i, t1);
            CCPoint p2 = _interpolate(i, t2);
            cpShape *seg = cpSegmentShapeNew(sp->staticBody, cpv(p1.x,p1.y), cpv(p2.x,p2.y), 0);
            seg->e = 1;
            seg->u = 1;
            cpSpaceAddStaticShape(sp, seg);
        }

        treemap[i] = i > 0 ? treemap[i-1] + oldn : 0;
        oldn = n;
        for(int j = 0; j < n; j++)
        {
            int idx = floor(CCRANDOM_0_1() * 7);
            float t = CCRANDOM_0_1();
            CCPoint p = _interpolate(i, t);
            CCPoint n = _interpolateNormal(i, t);
            CCSprite *tree = CCSprite::create(CCString::createWithFormat("tree%02d.png", idx)->getCString());
            tree->setAnchorPoint(ccp(0.5f, 0));
            tree->setScale(0.2);
            tree->setPosition(p);
            tree->setRotation(-CC_RADIANS_TO_DEGREES(n.getAngle()) + 90);
            tree->retain();
            trees.push_back(tree);
            
        }
    }
}

void Terrain::draw()
{
    ccVertex2F vp[4];
    ccTex2F vt[4];
//    unsigned int vi[] = {
//        0,1,2,
//        1,3,2
//    };
    
    int start = clampf(-getPositionX() / 200, 0, 100);
    
    CCGLProgram *shader = getShaderProgram();
    shader->use();
    shader->setUniformsForBuiltins();
    ccGLEnableVertexAttribs(kCCVertexAttribFlag_Position|kCCVertexAttribFlag_TexCoords);
    ccGLBindTexture2D(tex->getName());
    glVertexAttribPointer(kCCVertexAttrib_Position, 2, GL_FLOAT, GL_FALSE, 0, vp);
    glVertexAttribPointer(kCCVertexAttrib_TexCoords, 2, GL_FLOAT, GL_FALSE, 0, vt);
    
    for(int i = start; i < start+5 && i < 99; i++)
    {
        for(int j = 0; j < 10; j++)
        {
            float t1 = j / 10.0f;
            float t2 = (j+1) / 10.0f;
            CCPoint p1 = _interpolate(i, t1);
            CCPoint p2 = _interpolate(i, t2);
            vp[0] = vertex2(p1.x, p1.y);
            vp[1] = vertex2(p2.x, p2.y);
            vp[2] = vertex2(p1.x, 0);
            vp[3] = vertex2(p2.x, 0);
            float u1 = t1;
            float u2 = t2;
            vt[0] = tex2(u1, 1);
            vt[1] = tex2(u2, 1);
            vt[2] = tex2(u1, 0);
            vt[3] = tex2(u2, 0);
            glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
        }
    }
    
    start = treemap[start];
    for(int i = start; i < start+5*7 && i < trees.size(); i++)
    {
        CCSprite *tree = trees[i];
        tree->visit();
    }
}

CCPoint Terrain::_interpolate(int i, float t)
{
    float w = 200;
    float x = (i + t) * w;
    float dy = hills[i+1] - hills[i];
    float y = -sinf(t * M_PI * 2) * dy * 0.15 + hills[i] + t * dy;
    return ccp(x, y);
}

CCPoint Terrain::_interpolateNormal(int i, float t)
{
    CCPoint p1 = _interpolate(i, t-0.1f);
    CCPoint p2 = _interpolate(i, t+0.1f);
    CCPoint diff = p2-p1;
    return diff.getPerp();
}
