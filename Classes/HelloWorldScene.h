#ifndef __HELLOWORLD_SCENE_H__
#define __HELLOWORLD_SCENE_H__

#define CC_ENABLE_CHIPMUNK_INTEGRATION 1

#include "cocos2d.h"
#include "cocos-ext.h"
#include "chipmunk.h"
#include "Terrain.h"
#include "Sky.h"
#include <stdlib.h>

using namespace cocos2d;
using namespace extension;
using namespace std;

class HelloWorld : public cocos2d::CCLayer
{
    static HelloWorld *singleton;
    
    CCRenderTexture *m_rt;
    Terrain *m_terrain;
    Sky *m_sky, *m_grass, *m_rain;
    vector<CCSprite*> m_trees;
    CCLayer *m_treesLayer;
    CCSize m_screen;
    CCDirector *m_dir;
    CCPoint dragStart;
    cpSpace *m_space;
    CCPhysicsSprite *m_wheel;
    float m_shift;
    float m_scroll;
    
    void _generateBackground();
    void _update(float dt);
    
public:
    CREATE_FUNC(HelloWorld);
    static cocos2d::CCScene* scene();
    static cpSpace* getSpace() { return singleton->m_space; }
    
    virtual bool init();
    virtual bool ccTouchBegan(CCTouch *pTouch, CCEvent *pEvent);
    virtual void ccTouchMoved(CCTouch *pTouch, CCEvent *pEvent);
    virtual void ccTouchEnded(CCTouch *pTouch, CCEvent *pEvent);
    virtual void registerWithTouchDispatcher(void)
    {
        m_dir->getTouchDispatcher()->addTargetedDelegate(this, 0, false);
    }
};

#endif // __HELLOWORLD_SCENE_H__
